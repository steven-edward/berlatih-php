<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tentukan Nilai</title>
</head>

<body>
    <?php
    function ubah_huruf($string)
    {
        $abjad = "abcdefghijklmnopqrstuvwxyz";
        $tampung = "";
        for ($i=0; $i <strlen($string) ; $i++) { 
            $posisi = strpos($abjad ,substr($string,$i,1),1);
            $tampung .= substr($abjad,$posisi+1,1);
        }
        return $tampung."<br>";
    }

    // TEST CASES
    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu

    ?>
</body>

</html>