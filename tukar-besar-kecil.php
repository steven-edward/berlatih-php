<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tentukan Nilai</title>
</head>

<body>
    <?php
    function tukar_besar_kecil($string)
    {
        $strKecil = strtolower($string);
        $tampung = "";
        $strTemp = "";
        for ($i=0; $i < strlen($string) ; $i++) { 
            $str1= substr($string,$i,1);
            $str2= substr($strKecil,$i,1);
            if (strcmp($str1,$str2) == 0) {
                $strTemp .= strtoupper($str1);
            } else {
                $strTemp .= strtolower($str1);
            }
        }$tampung .= $strTemp;
        return $tampung."<br>";
    }

    // TEST CASES
    echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
    echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
    echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
    echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
    echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

    ?>
</body>

</html>